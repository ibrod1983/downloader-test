// var mysql      = require('mysql');
const fs = require('fs');
const Downloader = require('nodejs-file-downloader');
const { Qyu } = require('qyu');
const rpur = require('repeat-promise-until-resolved');
const { DownloaderHelper } = require('node-downloader-helper');

// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : '',
//   database : 'books'
// });

// connection.connect();
//  let imageNames;
// connection.query('SELECT * from products where image <> "placeholder.png"', function (error, results, fields) {
//   if(error){
//       console.log('err',error)
//   }
// //   console.log(results.length);
//   imageNames = results.map(r=>r.image)
// //   console.log(imageNames[677])
//  fs.writeFileSync('./images.json',JSON.stringify(imageNames))

// });

(async () => {
  let counter = 0;
  const q = new Qyu({ concurrency: 15 }); // Max 2 jobs can run concurrently

  const images = JSON.parse(fs.readFileSync('./images.json'));

  async function promiseFactory(image) {
    const downloader = new Downloader({
      url: `https://ibrod83.com/books/images/${image}`,
      fileName: image,
      directory: './images',
      // shouldBufferResponse:true

    })
    await downloader.download();
    counter++
    console.log(counter)


  }

  // function promiseFactory(image) {
  //   return new Promise(async(resolve) => {
  //     const dl = new DownloaderHelper(`https://ibrod83.com/books/images/${image}`, './images');
  //     await dl.start()
  //     dl.on('end',resolve);
  //   })
  // }

  q(images.slice(0,3500), async (image) => {
    await rpur(async () => {
      await promiseFactory(image)
    }, { timeout: 5000 }
    )
  });

  // for(let image of images){
  //   const downloader = new Downloader({
  //     url: `https://ibrod83.com/books/images/${image}`,
  //     fileName: image,
  //     directory: './images',
  //     // shouldBufferResponse:true

  //   })
  //   await downloader.download();
  // }




})()